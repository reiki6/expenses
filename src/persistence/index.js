import storage from 'localforage'

export function addExpense (expense) {
  storage.setItem(expense.id, expense).then(function (value) {
    // Do other things once the value has been saved.
    // console.log(value)
  }).catch(function (err) {
    // This code runs if there were any errors
    console.log(err)
  })
}

export function setDone (expense) {
  const item = storage.getItem(expense.id)
  item.done = expense.done
  storage.setItem(expense.id, expense)
}

export function getExpenses () {
  let list = []
  // The same code, but using ES6 Promises.
  storage.iterate(function (value, key, iterationNumber) {
    // console.log([key, value])
    list.push(value)
  }).then(function () {
    // console.log('Iteration has completed')
  }).catch(function (err) {
  // This code runs if there were any errors
    console.log(err)
  })
  return list
}

export function removeExpense (expense) {
  storage.removeItem(expense.id).then(function () {
  // Run this code once the key has been removed.
    console.log('Key is cleared!')
  }).catch(function (err) {
  // This code runs if there were any errors
    console.log(err)
  })
}
